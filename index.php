<?php
require_once ('animal.php');
require_once ("ape.php");
require_once ("frog.php");


$sheep = new animal("Shaun");

echo "Nama hewan : $sheep->name <br>"; // "shaun"
echo "Jumlah kaki : $sheep->legs <br>"; // 2
echo "Berdarah dingin : $sheep->cold_blooded <br><br>"; // false

// index.php
$sungokong = new ape("Kera Sakti");
echo "Nama hewan : $sungokong->name <br>"; // "shaun"
echo "Jumlah kaki : $sungokong->legs <br>"; // 2
echo "Berdarah dingin : $sungokong->cold_blooded <br>"; // false
$sungokong->yell(); // "Auooo"
echo "<br><br>";

$kodok = new frog("Buduk");
echo "Nama hewan : $kodok->name <br>"; // "shaun"
echo "Jumlah kaki : $kodok->legs <br>"; // 2
echo "Berdarah dingin : $kodok->cold_blooded <br>"; // false
$kodok->jump(); // "hop hop"

?>